# Contributing

> This document is based on Fractal's [CONTRIBUTING.md](https://gitlab.gnome.org/World/fractal/-/blob/7a35b0ac596b5a6c0704270bcb6ce4fa809d2947/CONTRIBUTING.md)
file, which is licensed under the GNU General Public License v3.0.

## Newcomers

[Bustle](https://gitlab.gnome.org/World/bustle) follows the [GNOME Newcomers workflow](https://wiki.gnome.org/Newcomers/BuildProject).
Check out these pages to learn how to contribute.

Here are also a few links to help you get started with Rust and the GTK Rust bindings:

- [Learn Rust](https://www.rust-lang.org/learn)
- [GUI development with Rust and GTK 4](https://gtk-rs.org/gtk4-rs/stable/latest/book)
- [gtk-rs website](https://gtk-rs.org/)

Don't hesitate to join [our Matrix room](https://matrix.to/#/#bustle:gnome.org) to come talk to us
and ask us any questions you might have.

## Build Instructions

### Setting up Your Development Environment

Bustle is written in Rust, so you will need to have at least Rust 1.75 and Cargo available on your
system.

If you're building Bustle with Flatpak (via GNOME Builder or the command line), you will need to
manually add the necessary remotes and install the required Freedesktop extensions:

```sh
# Add Flathub and the gnome-nightly repo
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --user --if-not-exists gnome-nightly https://nightly.gnome.org/gnome-nightly.flatpakrepo

# Install the gnome-nightly Sdk and Platform runtime
flatpak install --user gnome-nightly org.gnome.Sdk//master org.gnome.Platform//master

# Install the required rust-stable extension from Flathub
flatpak install --user flathub org.freedesktop.Sdk.Extension.rust-stable//24.08

# Install the required llvm extension from Flathub
flatpak install --user flathub org.freedesktop.Sdk.Extension.llvm18//24.08
```

If you are building the flatpak manually you will also need flatpak-builder on your system, or the
`org.flatpak.Builder` flatpak.

### Developing with Flatpak

Using [GNOME Builder](https://wiki.gnome.org/Apps/Builder) or [Flatpak VSCode Extension](https://github.com/bilelmoussaoui/flatpak-vscode)
is the recommended way of building and installing Bustle.

## Committing

Please follow the [GNOME commit message guidelines](https://handbook.gnome.org/development/commit-messages.html).

## Creating a Merge Request

Before submitting a merge request, make sure that [your fork is available publicly](https://gitlab.gnome.org/help/user/public_access.md),
otherwise CI won't be able to run.

Use the title of your commit as the title of your MR if there's only one. Otherwise, it should
summarize all your commits. If your commits do several tasks that can be separated, open several
merge requests.

In the details, write a more detailed description of what it does. If your changes include a change
in the UI or the UX, provide screenshots in both light and dark mode, and/or a screencast of the
new behavior.

Don't forget to mention the issue that this merge request solves or is related to, if applicable.
GitLab recognizes the syntax `Closes #XXXX` or `Fixes #XXXX` that will close the corresponding
issue accordingly when your change is merged.

We expect to always work with a clean commit history. When you apply fixes or suggestions,
[amend](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---amend) or
[fixup](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---fixupamendrewordltcommitgt)
and [squash](https://git-scm.com/docs/git-rebase#Documentation/git-rebase.txt---autosquash) your
previous commits that you can then [force push](https://git-scm.com/docs/git-push#Documentation/git-push.txt--f).
